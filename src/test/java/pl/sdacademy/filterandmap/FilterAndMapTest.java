package pl.sdacademy.filterandmap;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Wybierz imiona tych osob, ktore maja mniej niz 18 lat
 */
public class FilterAndMapTest {

    @Test
    public void shouldGetNamesWhenAgeIsLessThan18Java7() {
        //given
        Person anna = new Person("Anna", 16);
        Person jan = new Person("Jan", 40);
        Person ewa = new Person("Eva", 42);
        Person monika = new Person("Monika", 17);
        List<Person> collection = Arrays.asList(anna, ewa, jan, monika);

        //when
        List<String> result =
                FilterAndMapJava7.transform(collection);

        //then
        assertThat(result)
                .contains("Monika", "Anna")
                .doesNotContain("Jan", "Ewa");
    }

    @Test
    public void shouldGetNamesWhenAgeIsLessThan18Java8() {
        //given
        Person anna = new Person("Anna", 16);
        Person jan = new Person("Jan", 40);
        Person ewa = new Person("Eva", 42);
        Person monika = new Person("Monika", 17);
        List<Person> collection = Arrays.asList(anna, ewa, jan, monika);

        //when
        List<String> result =
                FilterAndMapJava8.transform(collection);

        //then
        assertThat(result)
                .contains("Monika", "Anna")
                .doesNotContain("Jan", "Ewa");
    }
}
