package pl.sdacademy.sum;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Zsumuj wszystkie elementy w kolekcji.
 */
public class SumTest {

    @Test
    public void shouldSumElementsWithJava7() {
        //given
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        //when
        int result = SumJava7.sum(numbers);

        //then
        assertThat(result).isEqualTo(1 + 2 + 3 + 4 + 5);
    }

    @Test
    public void shouldSumElementsWithJava8() {
        //given
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        //when
        int result = SumJava8.sum(numbers);

        //then
        assertThat(result).isEqualTo(1 + 2 + 3 + 4 + 5);
    }
}
