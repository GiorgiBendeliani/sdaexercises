package pl.sdacademy.statistics;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by rafal on 10/20/16.
 */
public class StatisticsTest {

    List<Person> collection;

    @Before
    public void setUp() throws Exception {
        Person monika = new Person("Monika", 4);
        Person jan = new Person("Jan", 40);
        Person ewa = new Person("Ewa", 42);
        collection = Arrays.asList(monika, ewa, jan);
    }

    //Java 7
    @Test
    public void getStatsShouldReturnAverageAge() {
        assertThat(StatsJava7.getStats(collection).getAverage())
                .isEqualTo((double) (4 + 40 + 42) / 3);
    }

    @Test
    public void getStatsShouldReturnNumberOfPeople() {
        assertThat(StatsJava7.getStats(collection).getCount())
                .isEqualTo(3);
    }

    @Test
    public void getStatsShouldReturnMaximumAge() {
        assertThat(StatsJava7.getStats(collection).getMax())
                .isEqualTo(42);
    }

    @Test
    public void getStatsShouldReturnMinimumAge() {
        assertThat(StatsJava7.getStats(collection).getMin())
                .isEqualTo(4);
    }

    @Test
    public void getStatsShouldReturnSumOfAllAges() {
        assertThat(StatsJava7.getStats(collection).getSum())
                .isEqualTo(40 + 42 + 4);
    }

    //Java8
    @Test
    public void getStatsShouldReturnAverageAgeJava8() {
        assertThat(StatsJava8.getStats(collection).getAverage())
                .isEqualTo((double) (4 + 40 + 42) / 3);
    }

    @Test
    public void getStatsShouldReturnNumberOfPeopleJava8() {
        assertThat(StatsJava8.getStats(collection).getCount())
                .isEqualTo(3);
    }

    @Test
    public void getStatsShouldReturnMaximumAgeJava8() {
        assertThat(StatsJava8.getStats(collection).getMax())
                .isEqualTo(42);
    }

    @Test
    public void getStatsShouldReturnMinimumAgeJava8() {
        assertThat(StatsJava8.getStats(collection).getMin())
                .isEqualTo(4);
    }

    @Test
    public void getStatsShouldReturnSumOfAllAgesJava8() {
        assertThat(StatsJava8.getStats(collection).getSum())
                .isEqualTo(40 + 42 + 4);
    }
}
