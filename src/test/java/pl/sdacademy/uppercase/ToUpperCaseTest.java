package pl.sdacademy.uppercase;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * zamien elementy z listy do na elementy z duzymi znakami
 */
public class ToUpperCaseTest {

    @Test
    public void shouldConvertCollectionElementsToUpperCaseWithJava7() {
        //given
        List<String> collection = Arrays.asList("My", "name", "is", "Jan", "Kowalski");

        //when
        List<String> result = ToUpperCaseJava7.transform(collection);

        //then
        List<String> expected = Arrays.asList("MY", "NAME", "IS", "JAN", "KOWALSKI");
        assertThat(result).hasSameElementsAs(expected);
    }

    @Test
    public void shouldConvertCollectionElementsToUpperCaseWithJava8() {
        //given
        List<String> collection = Arrays.asList("My", "name", "is", "Jan", "Kowalski");

        //when
        List<String> result = ToUpperCaseJava8.transform(collection);

        //then
        List<String> expected = Arrays.asList("MY", "NAME", "IS", "JAN", "KOWALSKI");
        assertThat(result).hasSameElementsAs(expected);
    }

}
