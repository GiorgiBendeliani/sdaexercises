package pl.sdacademy.max;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by rafal on 10/20/16.
 */
public class OldestPersonTest {

    @Test
    public void shouldFindOldestPersonWithJava7() {
        //given
        Person maria = new Person("Maria", 4);
        Person jolanta = new Person("Jolanta", 40);
        Person ewa = new Person("Ewa", 42);
        Person wiktor = new Person("Wiktor", 26);
        List<Person> collection = Arrays.asList(maria, jolanta, ewa, wiktor);

        //when
        Person result = OldestPersonJava7.getOldestPerson(collection);

        //then
        assertThat(result).isEqualToComparingFieldByField(ewa);
    }

    @Test
    public void shouldFindOldestPersonWithJava8() {
        //given
        Person maria = new Person("Maria", 4);
        Person jolanta = new Person("Jolanta", 40);
        Person ewa = new Person("Ewa", 42);
        Person wiktor = new Person("Wiktor", 26);
        List<Person> collection = Arrays.asList(maria, jolanta, ewa, wiktor);

        //when
        Person result = OldestPersonJava8.getOldestPerson(collection);

        //then
        assertThat(result).isEqualToComparingFieldByField(ewa);
    }
}
