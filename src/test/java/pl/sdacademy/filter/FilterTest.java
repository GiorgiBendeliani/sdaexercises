package pl.sdacademy.filter;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/*
 * Wyfiltruj elementy, ktorych dlugosc jest mniejsza niz 4 znaki
 */
public class FilterTest {

    @Test
    public void shouldFilterWithJava7Style() {
        //given
        List<String> collection = Arrays.asList("My", "name", "is", "Jan", "Kowalski");

        //when
        List<String> result = FilterJava7.filter(collection);

        //then
        List<String> expected = Arrays.asList("My", "is", "Jan");
        assertThat(result).hasSameElementsAs(expected);
    }

    /*@Test
    public void shouldFilterWithJava8Style() {
        //given
        List<String> collection = Arrays.asList("My", "name", "is", "Jan", "Kowalski");

        //when
        List<String> result = FilterJava8.filter(collection);

        //then
        List<String> expected = Arrays.asList("My", "is", "Jan");
        assertThat(result).hasSameElementsAs(expected);
    }*/

}
