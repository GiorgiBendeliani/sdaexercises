package pl.sdacademy.flatmap;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Splaszcz :) wielowymiarowa liste
 */
public class FlatMap {

    @Test
    public void shouldFlattenCollectionWithJava7() {
        //given
        List<List<String>> collection = Arrays.asList(Arrays.asList("Jan", "Kowalski"), Arrays.asList("Jerzy", "Von", "Nowak"));

        //when
        List<String> result = FlatMapJava7.transform(collection);

        //then
        List<String> expected = Arrays.asList("Jan", "Kowalski", "Jerzy", "Von", "Nowak");
        assertThat(result).hasSameElementsAs(expected);
    }

    @Test
    public void shouldFlattenCollectionWithJava8() {
        //given
        List<List<String>> collection = Arrays.asList(Arrays.asList("Jan", "Kowalski"), Arrays.asList("Jerzy", "Von", "Nowak"));

        //when
        List<String> result = FlatMapJava8.transform(collection);

        //then
        List<String> expected = Arrays.asList("Jan", "Kowalski", "Jerzy", "Von", "Nowak");
        assertThat(result).hasSameElementsAs(expected);
    }
}
