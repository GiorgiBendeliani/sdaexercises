package pl.sdacademy.max;

import java.util.Comparator;
import java.util.List;

/**
 * Created by rafal on 10/20/16.
 */
public class OldestPersonJava8 {
    public static Person getOldestPerson(List<Person> people) {
        return people
                .stream()
                .max(Comparator.comparing(Person::getAge))
                .get();
    }
}
