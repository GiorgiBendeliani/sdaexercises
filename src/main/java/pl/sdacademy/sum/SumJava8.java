package pl.sdacademy.sum;

import java.util.List;

/**
 * Created by rafal on 10/20/16.
 */
public class SumJava8 {
    public static int sum(List<Integer> numbers) {
        return numbers
                .stream()
                .reduce(0, (total, next) -> total + next);
    }
}
