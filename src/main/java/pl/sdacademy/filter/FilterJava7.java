package pl.sdacademy.filter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class FilterJava7 {

    public static List<String> filter(List<String> collection) {
        List<String> result = new ArrayList<>(collection.size());
        for(String str : collection){
            if(str.length()<4){
                result.add(str);
            }
        }
        return result;
    }
}
