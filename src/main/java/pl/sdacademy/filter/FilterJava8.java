package pl.sdacademy.filter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rafal on 10/20/16.
 */
public class FilterJava8 {
    public static List<String> filter(List<String> collection) {
        return collection
                .stream()
                .filter(str -> str.length()<4)
                .collect(Collectors.toList());
    }
}
